"""
Dynap-SE Controller

A set of tools for controlling the Dynap-SE neuromorphic processor via cortexcontrol.

Created by Mattias Nilsson
"""
import CtxDynapse
import PyCtxUtils


class DynapseController:

    def __init__(self):
        """Instantiate classes from cortexcontrol for interaction and control of Dynap-SE board
        """
        self.CtxDynapse = CtxDynapse  # cortexcontrol Dynap-SE module

        self.dynapse = self.CtxDynapse.dynapse           # Low-level access to a Dynap-SE
        self.syn_types = self.CtxDynapse.DynapseCamType  # Holding class for synapses types
        self.model = self.CtxDynapse.model               # Software model of physical board
        #self.virtual_model = CtxDynapse.VirtualModel()   # Software model of the virtual chip (ID = 128)


        self.groups = self.model.get_bias_groups()
        self.fpga_modules = self.model.get_fpga_modules()

        self.poisson_gen = self.fpga_modules[0]
        self.spike_gen = self.fpga_modules[1]

        ADDR_NUM_BITS = 15
        ISI_NUM_BITS = 16

        self.max_fpga_len = 2 ** ADDR_NUM_BITS - 1
        self.max_isi = 2 ** ISI_NUM_BITS - 1

        self.isi_base = 900
        self.unit_mult = self.isi_base / 90
        """Comment:
            Dynap-SE clock cycle is 1/(90 Mhz) = 11.11 ns
        """

        self.spike_gen.set_isi_multiplier(self.isi_base)


    """Silencing methods
    
    Methods for shutting down neuronal activity in the cores of the Dynap-SE.
    """
    def silence_neurons_core(self, core_id):
        """Silence neurons of core
        
        Args:
            core_id (int):  [0,16)
        """
        self.groups[core_id].set_bias("IF_DC_P", 0, 0)
        self.groups[core_id].set_bias("IF_TAU1_N", 255, 7)
        self.groups[core_id].set_bias("IF_TAU2_N", 255, 7)

    def silence_synapses_core(self, core_id):
        """Silence synapses of core

        Args:
            core_id (int):  [0,16)
        """
        self.groups[core_id].set_bias("NPDPIE_TAU_F_P", 255, 7)
        self.groups[core_id].set_bias("NPDPIE_TAU_S_P", 255, 7)
        self.groups[core_id].set_bias("NPDPII_TAU_F_P", 255, 7)
        self.groups[core_id].set_bias("NPDPII_TAU_S_P", 255, 7)

    def silence_adaptation_core(self, core_id):
        """Switch off spike frequency adaptation in core

        Args:
            core_id (int):  [0,16)
        """
        self.groups[core_id].set_bias("IF_AHTAU_N", 255, 7)
        self.groups[core_id].set_bias("IF_AHW_P", 0, 0)

    def silence_core(self, core_id):
        """Silence all activity in core

        Args:
            core_id (int):  [0,16)
        """
        self.silence_neurons_core(core_id)
        self.silence_synapses_core(core_id)
        self.silence_adaptation_core(core_id)

    def silence_everything(self):
        """Silence all activity in all cores
        """
        for core_id in range(16):
            self.silence_core(core_id)


    """Bias-file methods
    
    Methods for handling files with bias values.
    """
    def save_biases(self, filename):
        """Save biases to file
        
        Args:
            filename (str): "*.py"
        """
        PyCtxUtils.save_biases(filename)
        
    def load_biases(self, filename):
        """Load biases from file
        
        Args:
            filename (str): "*.py"
        """
        exec(open(filename).read())
        
    def copy_biases(self, source_core_id, target_core_id):
        """Copy all biases from one core to another

        Args:
            source_core_id (int):   [0,16)
            target_core_id (int):   [0,16)
        """
        source_biases = self.groups[source_core_id].get_biases()

        for bias in source_biases:
            self.groups[target_core_id].set_bias(
                bias.bias_name,
                bias.fine_value,
                bias.coarse_value
            )


    """CAM methods
    
    Low-level methods for writing input-routing data to the CAMs of Dynap-SE neurons.
    
    NOTE: Cortexcontrol will not keep track of changes that are made using these functions!
    """
    def clear_cams(self, chip_id):
        """Clear CAMs of chip

        Args:
            chip_id (int): [0,4)
        """
        self.dynapse.clear_cam(chip_id)

    def clear_all_cams(self):
        """Clear CAMs of whole board
        """
        for chip_id in range(4):
            self.dynapse.clear_cam(chip_id)

    def write_cam(self, pre_neuron_id, post_neuron_id, cam_id, syn_type_id):
        """Write input-routing data to CAM of a neuron

        Args:
            pre_neuron_id (int):    Chip-level source-neuron ID [0:1024)
            post_neuron_id (int):   Chip-level destination-neuron ID [0:1024)
            cam_id (int):           Neuron-level CAM ID [0,64)
            syn_type_id (int):      Synapse type ID [0,4)
        """
        if syn_type_id == 0:
            syn_type = self.syn_types.SLOW_INH

        elif syn_type_id == 1:
            syn_type = self.syn_types.FAST_INH

        elif syn_type_id == 2:
            syn_type = self.syn_types.SLOW_EXC

        elif syn_type_id == 3:
            syn_type = self.syn_types.FAST_EXC

        else:
            print("Error: Invalid synapse type ID")
            return

        self.dynapse.write_cam(pre_neuron_id, post_neuron_id, cam_id, syn_type)


    """SRAM methods
    
    Low-level methods for writing output-routing data to the SRAMs of Dynap-SE neurons.
    
    NOTE: Cortexcontrol will not keep track of changes that are made using these functions!
    """
    def clear_sram(self, chip_id):
        """Clear SRAMs of chip

        Args:
            chip_id (int): [0,4)
        """
        self.dynapse.clear_sram(chip_id)
    
    def clear_all_srams(self):
        """Clear SRAMs of whole board
        """
        for chip_id in range(4):
            self.dynapse.clear_sram(chip_id)

    def write_sram(self, pre_neuron_id, sram_id):
        """Write output address data to SRAM of neuron

        Args:
            pre_neuron_id (int):    Chip-level neuron ID [0,1024)
            sram_id (int):          Neuron-level SRAM ID [0,4) (#O is used for USB)
        """
        # Chip routing parameters - send to the same chip
        dx = 0
        sx = 0
        dy = 0
        sy = 0

        # Target-core mask (one-hot binary) - send to all cores (1111 --> 15)
        core_mask = 15

        # Set virtual core ID to equal real core ID
        virtual_core_id = int(pre_neuron_id / 256)

        self.dynapse.write_sram(pre_neuron_id, sram_id, virtual_core_id, sx, dx, sy, dy, core_mask)


    """FPGA spike-generator methods
    
    Methods for controlling the FPGA spike-generator of the Dynap-SE.
    """
    def write_const_spike_gen(self, source_nrn_id, rate):
        """Configure spike-generator for constant-ISI stimulation.

        Args:
            source_nrn_id (int):    Virtual source neuron ID [0,1024)
            rate (int):             Constant spike rate
        """
        print("unit_mult = " + str(self.unit_mult))
        print("isi_base = " + str(self.isi_base))

        # Interspike interval (Dynap-SE format)
        isi = int(((rate * 1e-6)**(-1)) / self.unit_mult)

        # Stimulus-event object
        fpga_event = self.CtxDynapse.FpgaSpikeEvent()

        # Event data
        fpga_event.target_chip = 0            # Target chip
        fpga_event.core_mask = 15             # Target cores
        fpga_event.neuron_id = source_nrn_id  # Virtual source-neuron ID

        # Program spike-generator
        self.spike_gen.preload_stimulus([fpga_event])
        self.spike_gen.set_variable_isi(False)
        self.spike_gen.set_isi_multiplier(self.isi_base)
        self.spike_gen.set_isi(isi)
        self.spike_gen.set_repeat_mode(True)

        self.spike_gen.start()

    def stop_const_spike_gen(self):
        """End constant-rate spike-generation
        """
        self.spike_gen.set_repeat_mode(False)  # Used instead of spike_gen.stop(), which seems not to work as intended 

    def make_fpga_event_list(self, isi_list, source_nrn_ids, target_chip):
        """Create list of FpgaSpikeEvent objects

        Args:
            isi_list (list):                List of ISIs
            source_nrn_ids (list):          List of source-neuron IDs
            target_chip (one-hot binary):   Target chip ID [1,2,4,8]

        Returns:
            fpga_events (FpgaSpikeEvent):   List of FpgaSpikeEvent objects
        """
        print("Creating FPGA event list...")
        fpga_events = []

        for i, isi in enumerate(isi_list):

            fpga_event = self.CtxDynapse.FpgaSpikeEvent()  # Event object

            # Event data
            fpga_event.core_mask = 15  # one-hot binary
            fpga_event.target_chip = target_chip
            fpga_event.neuron_id = int(source_nrn_ids[i])
            fpga_event.isi = int(isi * 1e6 / self.unit_mult)

            fpga_events.append(fpga_event)  # Add event to list

        print("...done")
        return fpga_events

    def write_variable_spike_gen(self, isi_list, source_nrn_ids, target_chip):
        """Configure spike-generator for variable-ISI stimulus.
        
        Args:
            isi_list (list):                List of ISIs (s)
            source_nrn_ids (list):          List of source-neuron IDs
            target_chip (one-hot binary):   Target chip ID [1,2,4,8]
        """
        # Make FPGA event list
        fpga_events = self.make_fpga_event_list(isi_list, source_nrn_ids, target_chip)

        # Program spike-generator
        self.spike_gen.preload_stimulus(fpga_events)
        self.spike_gen.set_isi_multiplier(self.isi_base)
        self.spike_gen.set_variable_isi(True)
        self.spike_gen.set_repeat_mode(True)


    """Miscellaneous methods
    """
    def monitor_neuron(self, chip_id, neuron_id):
        """Select neuron to monitor in chip

        Args:
            chip_id (int):      Chip ID [0,4)
            neuron_id (int):    Neuron ID within chip [0:1024)
        """
        self.dynapse.monitor_neuron(chip_id, neuron_id)

        print(
            "Monitoring neuron "
            + str(neuron_id % 256)
            + " in core "
            + str(int(neuron_id / 256))
            + " in chip "
            + str(chip_id)
        )

    def _get_global_neuron_id(self, local_neuron_id, core_id, chip_id):
        """Get global neuron ID (within whole Dynap-SE board)

        Args:
            local_neuron_id (int):  Core-level neuron ID [0,256)
            core_id (int):          [0,4)
            chip_id (int):          [0,4)
        Return:
            global_neuron_id (int): Neuron ID within whole board [0,4096)
        """
        neurons_per_core = 256
        neurons_per_chip = 1024

        global_neuron_id = local_neuron_id \
                           + neurons_per_core * core_id \
                           + neurons_per_chip * chip_id

        return global_neuron_id

    def events_to_raster(self, spike_list):
        """Create raster-plot data from list of CtxDynapse.Spike objects

        Args:
            spike_list (list):                  List of CtxDynapse.Spike spike-event objects
        Returns:
            neuron_ids (list or numpy array):   List of neuron IDs
            spike_times (list or numpy array):  List of spike times (s)
        """
        neuron_ids = []
        spike_times = []

        for spike in spike_list:

            glob_neuron_id = self._get_global_neuron_id(
                spike.neuron.get_neuron_id(),
                spike.neuron.get_core_id(),
                spike.neuron.get_chip_id()
            )
            neuron_ids.append(glob_neuron_id)
            spike_times.append(spike.timestamp * 1e-6)  # convert from ms to s

        return neuron_ids, spike_times

