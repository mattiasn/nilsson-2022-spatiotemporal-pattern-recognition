import numpy as np
import matplotlib.pyplot as plt
import random


class Stimulus:
    """Class for storing generated stimulus spike-trains.
    """
    def __init__(
        self,
        neuron_id,
        weight_de,
        source_ids,
        pre_times,
        post_times
    ):   
        self.neuron_id = neuron_id
        self.weight_de = weight_de
        self.source_ids = source_ids
        self.pre_times = pre_times
        self.post_times = post_times
        
        
class CamStimulus:
    """Class for storing generated stimulus spike-trains (CAM sampling version).
    """
    def __init__(
        self,
        neuron_id,
        index,
        source_ids,
        pre_times,
        post_times
    ):
        self.neuron_id = neuron_id
        self.index = index
        self.source_ids = source_ids
        self.pre_times = pre_times
        self.post_times = post_times
        
        
def events_to_lists(
    time_mat,
    neuron_mat,
    start_delay = 0
):
    """Convert event-data matrices to lists containing the corresponding ISIs in sequence.

    Args:
        time_mat (NumPy array):     Event time-stamp matrix
        neuron_mat (NumPy array):   Event neuron matrix
        start_delay (int):          Temporal delay before first spike (to separate repeated stimulations)

    Returns:
        isi_list (list):            Interspike intervals for each event
        source_nrn_ids (list):      Source-neuron IDs for each event
    """
    
    # Flatten event-data matrices
    time_arr = np.array(time_mat).flatten()
    neuron_arr = np.array(neuron_mat).flatten()

    # Sort event-data by global time
    sort_ind = np.argsort(time_arr)
    time_arr = time_arr[sort_ind]
    neuron_arr = neuron_arr[sort_ind]

    # Extract interspike intervals
    isi_arr = np.diff(
        time_arr,
        prepend = (time_arr[0] - start_delay)  # Prepend for delay of first spike
    )

    # Convert arrays to Python lists
    isi_list = isi_arr.tolist()
    source_nrn_ids = neuron_arr.tolist()

    return isi_list, source_nrn_ids


def plot_spike_train(
    num_cols,
    time_stamps,
    channels
):
    """Plot spike-train (time-stamps vs. input-channels).

    Args:
        num_cols (int):                     Number of network columns
        time_stamps (list or NumPy array):  List of event time-stamps
        channels: (list or NumPy array):    List of event channels
    """
    # Input spike-data plot
    plt.scatter(
        time_stamps * 1e3,
        channels
    )

    plt.xlabel('Time (ms)')
    plt.ylabel('Input Neuron')

    # plt.xticks(np.arange(0, time_array[-1]*1e3, step=1))
    plt.yticks(range(0, num_cols+1))

    plt.savefig('input_spikes.eps')


def gen_linear_pulse_sweep(
    num_cols,
    t_ch,
    t_pls,
    num_spk,
    start_delay
):
    """Generate spike-pulse input for linear frequency sweep.

    Args:
        num_cols (int):     Number of network columns
        t_ch (int):         Channel activation interval (s)
        t_pls (int):        Pulse duration (s)
        num_spk (int):      Number of spikes per pulse
        start_delay (int):  Temporal delay before first spike (to separate repeated stimulations)


    Returns:
        isi_list (list):        Event interspike intervals
        source_nrn_ids (list):  Event source-neuron IDs
    """
    # Pulse interspike interval (s)
    isi_pls=t_pls / (num_spk-1)

    # Stimulation end-time (s)
    t_end=(num_cols-1) * t_ch + t_pls

    # Event-data matrices
    time_mat=np.zeros((num_cols, num_spk))
    channel_mat=np.zeros_like(time_mat)

    # Loop over input channels
    for i, channel in enumerate(range(1, num_cols+1)):

        # Define pulse time
        start_time = (i) * t_ch
        stop_time = start_time + t_pls

        # Channel spike-times
        times_ch = np.linspace(start_time, stop_time, num=num_spk)

        # Store in matrices
        time_mat[i, :] = times_ch
        channel_mat[i, :] = channel

    # Convert time-events to ordered interspike intervals
    isi_list, source_nrn_ids = events_to_lists(
        time_mat,
        channel_mat,
        start_delay
    )

    # Return sequence of events as lists
    return isi_list, source_nrn_ids


def gen_linear_spike_sweep(
    num_cols,
    t_ch,
    start_delay
):
    """Generate single-spike input for linear frequency sweep.

    Args:
        num_cols (int):     Number of network columns
        t_ch (int):         Channel activation interval (s)
        start_delay (int):  Temporal delay before first spike (to separate repeated stimulations)


    Returns:
        isi_list (list):        Event interspike intervals
        source_nrn_ids (list):  Event source-neuron IDs
    """
    # Calculate number of instances of network
    num_nets = int(255 / num_cols)    # Neuron #0 not used!

    # Stimulation end-time (s)
    t_end = (num_cols-1) * t_ch

    # Event-data matrices
    time_mat = np.zeros(num_nets*num_cols)
    channel_mat = np.zeros_like(time_mat)

    # Loop over network instances | 0--50
    for net_idx in range(num_nets):
        #print("Network index:\t" + str(net_idx))

        # Loop over input channels | 0--4
        for channel in range(num_cols):

            # Get global index of current input channel
            glob_channel = net_idx*num_cols + (channel+1)
            #print("Global channel:\t" + str(glob_channel))

            # Update time-variable
            time = channel * t_ch

            # Store event-data in matrices
            time_mat[glob_channel-1] = time
            channel_mat[glob_channel-1] = glob_channel

    # Convert time-events to ordered interspike intervals
    isi_list, source_nrn_ids = events_to_lists(
        time_mat,
        channel_mat,
        start_delay
    )

    # Return sequence of events as lists
    return isi_list, source_nrn_ids


def gen_rand_rf_stim(num_nrns, num_lat):
    """Generate randomly perturbed receptive-field stimuli.

    Args:
        num_nrns (list):    Number of receiving (B2) neurons.
        num_lat (int):      Number of lateral inputs per B2 neuron.
        
    Returns:
        spk_times (NumPy array):    Stimulus spike times
        spk_sources (NumPy array):  Stimulus source-neuron addresses
    """
    # ISI range
    a = 1e-3
    b = 50e-3
            
    # Spike times
    spk_times = np.array([])
    
    # Loop over receiving neurons
    for nrn in range(num_nrns):
            
        # Loop over lateral (delayed) input spikes
        for spike in range(num_lat):
            
            # Generate random interspike interval
            isi = random.uniform(a,b)
            spk_times = np.append(spk_times, -isi)
            
        # Direct excitation event - at "reference time"
        spk_times = np.append(spk_times, 0)
        
    # Spike source addresses
    spk_sources = np.arange(1, len(spk_times)+1)
    
    return spk_times, spk_sources