def program_srams(core_ids):
    """Write the SRAMs of the Dynap-SE according to the STC network.
    
    Args:
        core_ids (dictionary):    Dictionary with core IDs of the different STC neurons.
        
    """
    # SRAM ID [0,3] (#0 is used for USB)
    sram_id = 1

    # Loop over neurons in B1 core
    for neuron_id in range(256*core_ids['B1'],
                               256*core_ids['B1'] + 256):
        
        # Program SRAM of neuron
        dc.write_sram(neuron_id, sram_id)
    

def program_cams(
    num_cols,
    num_neigh,
    core_ids,
    neuron_ids,
    syn_types
):
    """Write the CAMs of the Dynap-SE according to the STC network.
    
    Args:
        num_cols (int):     Number of network columns
        num_neigh (int):    Number of connected neighboring columns - on each side
        core_ids (dict):    Dynap-SE core IDs for the different neuron populations
        neuron_ids (dict):  IDs of hardware neurons for network implementation
    """
    # Loop over network columns
    for i in range(num_cols):

        # Column neuron IDs
        neuron_ids['A'] = 1  # for input testing
        #neuron_ids['A'] = 256*core_ids['A'] + i+1  # don't use neuron 0 (?)

        neuron_ids['B1'] = 256*core_ids['B1'] + i
        neuron_ids['B2'] = 256*core_ids['B2'] + i

        # A --> B1, fast exc.
        dc.write_cam(
            neuron_ids['A'],    # Pre-syn. neuron ID
            neuron_ids['B1'],   # Post-syn. neuron ID
            0,                  # CAM ID
            syn_types['exc_f']  # Synapse type ID
        )

        # A --> B2, fast exc.
        dc.write_cam(
            neuron_ids['A'],    # Pre-syn. neuron ID
            neuron_ids['B2'],   # Post-syn. neuron ID
            0,                  # CAM ID
            syn_types['exc_f']  # Synapse type ID
        )
        
        # B1 --> B2, shunting ("slow") inh.
        dc.write_cam(
            neuron_ids['B1'],   # Pre-syn. neuron ID
            neuron_ids['B2'],   # Post-syn. neuron ID
            1,                  # CAM ID
            syn_types['inh_s']  # Synapse type ID
        )

        # Write B2 CAMs for disynaptic delays
        cam_id_B2 = 2

        # Loop over neighboring columns
        for j in range (i-num_neigh, i+num_neigh + 1):

            # Check validity of neighbor column ID
            if (j >= 0) and (j != i) and (j < num_cols):

                # Get neuron ID for neighboring B1
                neuron_ids['B1_nb'] = 256*core_ids['B1'] + j

                # Delay element CAMs | B1_nb --> B2
                dc.write_cam(
                    neuron_ids['B1_nb'],  # Pre-syn. neuron ID
                    neuron_ids['B2'],     # Post-syn. neuron ID
                    cam_id_B2,            # CAM ID
                    syn_types['exc_s']    # Synapse type ID
                )
                cam_id_B2 += 1
                
                dc.write_cam(
                    neuron_ids['B1_nb'],  # Pre-syn. neuron ID
                    neuron_ids['B2'],     # Post-syn. neuron ID
                    cam_id_B2,            # CAM ID
                    syn_types['inh_f']    # Synapse type ID
                )
                cam_id_B2 += 1

    print("...done")