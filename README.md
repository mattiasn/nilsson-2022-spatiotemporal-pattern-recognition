# Nilsson 2022 Spatiotemporal Pattern Recognition

Code for implementing and mapping the randomization-based receptive fields on the DYNAP-SE mixed-signal neuromorphic processor presented in the paper "Spatiotemporal Pattern Recognition in Single Mixed-Signal VLSI Neurons with Heterogeneous Dynamic Synapses" by Nilsson et al., 2022.
